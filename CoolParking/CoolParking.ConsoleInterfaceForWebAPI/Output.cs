﻿using System;
using System.Net.Http;
using System.Text;
using CoolParking.BL.Models;

namespace CoolParking.ConsoleInterfaceForWebAPI
{
    class Output
    {
        private static HttpClient client = new HttpClient();
        
        public static void GetBalance()
        {
            var response = client.GetAsync("api/parking/balance");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = decimal.Parse(data.Result);
            
            Console.WriteLine($"Parking balance : {result}"); 
        }

        public static void GetCapacity()
        {
            var response = client.GetAsync("api/parking/capacity");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = int.Parse(data.Result);
            
            Console.WriteLine($"Capacity of parking : {result}");
        }

        public static void GetFreePlaces()
        {
            var response = client.GetAsync("api/parking/freePlaces");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = int.Parse(data.Result);
            
            Console.WriteLine($"Number of free places : {result}");
        }

        public static void GetVehicles()
        {
            var response = client.GetAsync("api/vehicles");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = data.Result;
            
            Console.WriteLine($"List of Vehicles located in the Parking lot : {result}");
        }

        public static void GetVehicle()
        {
            Console.WriteLine("Enter id:");
            var id = Console.ReadLine();
            
            var response = client.GetAsync($"api/vehicles/{id}");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = data.Result;
            
            Console.WriteLine($"The transaction history : {result}");
        }
        
        public static void AddVehicle()
        {
            Console.WriteLine("Choice VehicleType:\n 1.PassengerCar \n 2.Truck \n 3.Bus \n 4.Motorcycle");
            if (int.TryParse(Console.ReadLine(), out int n) && (n > 0 && n < 5))
            {
                Console.WriteLine("Balance : ");
                if (decimal.TryParse(Console.ReadLine(), out decimal c) && c > 0)
                    try
                    {
                        HttpContent content = new StringContent("{" + 
                                                                $"\"Id\": \"{Vehicle.GenerateRandomRegistrationPlateNumber()}\", " +
                                                                $"\"VehicleType\": \"{(VehicleType) n}\", " +
                                                                $"\"Balance\": \"{c}\"" +
                                                                "}", Encoding.UTF8, "application/json");

                        var response = client.PostAsync("api/vehicles", content);
                        var data = response.Result.Content.ReadAsStringAsync();

                        var result = data.Result;
                        Console.WriteLine("Vehicle was added");
                    }
                    catch (InvalidOperationException)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("No free places");
                    }
                    catch (ArgumentException)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Car with this id has already been added");
                    }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid input!");
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid input!");
            }
        }

        public static void RemoveVehicle()
        {
            Console.WriteLine("Enter id:");
            var id = Console.ReadLine();
            
            var response = client.DeleteAsync($"api/vehicles/{id}");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = data.Result;
            
            Console.WriteLine("Vehicle was removed");
        }

        public static void GetLastTransactions()
        {
            var response = client.GetAsync("api/transactions/last");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = data.Result;
            
            Console.WriteLine($"Last transactions for the current period : {result}");
        }
        
        public static void GetTransactionHistory()
        {
            var response = client.GetAsync("api/transactions/all");
            var data = response.Result.Content.ReadAsStringAsync();

            var result = data.Result;
            
            Console.WriteLine($"The transaction history : {result}");
        }
        
        public static void TopUpVehicleBalance()
        {
            Console.WriteLine("Enter id:");
            var id = Console.ReadLine();

            Console.WriteLine("Enter amount of money:");
            int.TryParse(Console.ReadLine(), out int sum);

            try
            {
                HttpContent content = new StringContent("{" + 
                                                        $"\"id\": \"{id}\"," +
                                                        $"\"sum\": {sum} " +
                                                        "}", Encoding.UTF8, "application/json");
                
                var response = client.PutAsync("api/vehicles", content);
                var data = response.Result.Content.ReadAsStringAsync();

                var result = data.Result;
                
                Console.WriteLine("Vehicle was added");
            }
            catch (InvalidOperationException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Balance is negative");
            }
            catch (ArgumentException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id");
            }
        }
        
        public static void Main()
        {
            client.BaseAddress = new Uri("https://localhost:7021/");
            
            //Menu
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Welcome to the CoolParking! \n");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Available actions: \n");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(
                "1.  Display the current Parking balance. \n" +
                "2.  Display the capacity of parking. \n" +
                "3.  Display the number of free parking places. \n" +
                "4.  Display the list of Vehicles \n" +
                "5.  Display vehicle by id. \n" +
                "6.  Put the Vehicle in the Parking. \n" +
                "7.  Pick up the Vehicle from Parking. \n" +
                "8.  Display last transactions for the current period. \n" +
                "9.  Display the transaction history. \n" +
                "10. Top up vehicle balance. \n" +
                "11. Exist \n");
            
            int choice;

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("\nSelect an action using a number");
                Console.ResetColor();

                while (true)
                    if (int.TryParse(Console.ReadLine(), out int number) && (number > 0 && number < 12))
                    {
                        choice = number;
                        break;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Invalid input!");
                        Console.ResetColor();
                    }

                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;

                switch (choice)
                {
                    case 1: GetBalance();
                            break;
                    case 2: GetCapacity();
                            break;
                    case 3: GetFreePlaces();
                            break;
                    case 4: GetVehicles();
                            break;
                    case 5: GetVehicle();
                            break;
                    case 6: AddVehicle();
                            break;
                    case 7: RemoveVehicle();
                            break;
                    case 8: GetLastTransactions();
                            break;
                    case 9: GetTransactionHistory();
                            break;
                    case 10: TopUpVehicleBalance();
                            break;
                    case 11: Environment.Exit(0); 
                            break;
                }
                Console.ResetColor();
            }
        }
    }
}
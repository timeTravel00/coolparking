﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer Timer { get; set; }
        public double Interval { get; set; }
        
        public event ElapsedEventHandler Elapsed;
        
        public void Start()
        {
            Timer = new Timer(Interval);
            Timer.Elapsed += Timer_Elapsed;
            Timer.Enabled = true;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(sender, e);
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }
    }
}

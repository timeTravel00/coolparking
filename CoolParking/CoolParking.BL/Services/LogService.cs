﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        
        public void Write(string logInfo)
        {
            using (StreamWriter file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            };
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException();

            using (StreamReader file = new StreamReader(LogPath))
            {
                return file.ReadToEnd();
            };
        }
    }
}

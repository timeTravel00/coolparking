﻿using System;
using System.Timers;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public ITimerService WithdrawTimer { get; set; }
        public ITimerService LogTimer { get; set; }
        public ILogService LogService { get; set; }
        public Parking Parking { get; }
        public List<TransactionInfo> TransactionInfo { get; set; }
        
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
            Parking = Parking.SingletonParking;
            Parking.Balance = Settings.InitBalanceParking;
            Parking.Vehicles = new List<Vehicle>() { Capacity = Settings.CapacityParking };
            TransactionInfo = new List<TransactionInfo>();

            WithdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            WithdrawTimer.Interval = Settings.BillingPeriod;

            LogTimer.Elapsed += LogTimer_Elapsed;
            LogTimer.Interval = Settings.LoggingPeriod;


            WithdrawTimer.Start();
            LogTimer.Start();
        }
        
        private void WithdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach(Vehicle vehicle in Parking.Vehicles)
                TransactionInfo.Add(new TransactionInfo(vehicle.Id, Withdraw(vehicle), DateTime.Now));
        }

        private void LogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            LogService.Write(new string('-', 20));

            foreach (TransactionInfo transactionInfo in TransactionInfo)
                LogService.Write(transactionInfo.ToString());
           
            TransactionInfo = new List<TransactionInfo>();
        }

        private decimal Withdraw(Vehicle vehicle)
        {
            decimal tariff;
            switch (vehicle.VehicleType)
            {
                case VehicleType.PassengerCar: tariff = Settings.PassengerCarTariff; break;
                case VehicleType.Truck: tariff = Settings.TruckTariff; break;
                case VehicleType.Bus: tariff = Settings.BusTariff; break;
                case VehicleType.Motorcycle: tariff = Settings.MotorcycleTariff; break;
                default: throw new Exception();
            }

            if (vehicle.Balance - tariff >= 0)
            {
                vehicle.Balance -= tariff;
                Parking.Balance += tariff;
                return tariff;
            }
            if (vehicle.Balance - tariff < 0 && vehicle.Balance > 0)
            {
                vehicle.Balance -= vehicle.Balance + (tariff - vehicle.Balance) * Settings.CoefficientPenalty;
                Parking.Balance += vehicle.Balance + (tariff - vehicle.Balance) * Settings.CoefficientPenalty;
                return vehicle.Balance + (tariff - vehicle.Balance) * Settings.CoefficientPenalty;
            }

            vehicle.Balance -= tariff * Settings.CoefficientPenalty;
            Parking.Balance += tariff * Settings.CoefficientPenalty;
            return tariff * Settings.CoefficientPenalty;
        }

        public decimal GetBalance() => Parking.Balance;

        public int GetCapacity() => Parking.Vehicles.Capacity;

        public int GetFreePlaces() => Parking.Vehicles.Capacity - Parking.Vehicles.Count;

        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(Parking.Vehicles);

        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.Vehicles.Count == Settings.CapacityParking)
                throw new InvalidOperationException();

            foreach (Vehicle v in GetVehicles())
                if (vehicle.Id == v.Id)
                    throw new ArgumentException();

            Parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            foreach (Vehicle vehicle in Parking.Vehicles)
                if (vehicle.Id == vehicleId) {
                    if (vehicle.Balance > 0)
                    {
                        Parking.Vehicles.Remove(vehicle);
                        return;
                    }
                    else{
                        throw new InvalidOperationException();
                    }
                }

            throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException();

            foreach (Vehicle vehicle in Parking.Vehicles)
                if(vehicle.Id == vehicleId) {
                    vehicle.Balance += sum;
                    return;
                }

            throw new ArgumentException();

        }

        public TransactionInfo[] GetLastParkingTransactions() => TransactionInfo.ToArray();

        public string ReadFromLog() => LogService.Read();

        public void Dispose()
        {
            WithdrawTimer.Dispose();
            LogTimer.Dispose();
        }
    }
}

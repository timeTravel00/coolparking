﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitBalanceParking { get; }
        public static int CapacityParking { get; }
        public static int BillingPeriod { get; }
        public static int LoggingPeriod { get; }

        public static decimal PassengerCarTariff { get; }
        public static decimal TruckTariff { get; }
        public static decimal BusTariff { get; }
        public static decimal MotorcycleTariff { get; }

        public static decimal CoefficientPenalty { get; }

        static Settings()
        {
            InitBalanceParking = 0;
            CapacityParking = 10;
            BillingPeriod = 5000;
            LoggingPeriod = 60000;

            PassengerCarTariff = 2;
            TruckTariff = 5;
            BusTariff = 3.5m;
            MotorcycleTariff = 1;

            CoefficientPenalty = 2.5m;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> parking = new Lazy<Parking>(
            () => new Parking(Settings.InitBalanceParking, new List<Vehicle>() {Capacity = Settings.CapacityParking })
        ); 
        
        public static Parking SingletonParking => parking.Value;
        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles { get; internal set; }

        private Parking(decimal balance, List<Vehicle> vehicles)
        {
            Balance = balance;
            Vehicles = vehicles;
        }
    }
}

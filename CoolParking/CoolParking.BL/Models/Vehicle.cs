﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly string format = @"\D{2}-\d{4}-\D{2}";
        
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }     
        
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0 || !Regex.IsMatch(id, format))
                throw new ArgumentException();

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        
        public Vehicle() { }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rand = new Random();
            StringBuilder strId = new StringBuilder(10);

            strId.Append(
                $"{(char)rand.Next('A', 'Z')}{(char)rand.Next('A', 'Z')}" +
                '-' +
                $"{rand.Next(0, 9)}{rand.Next(0, 9)}{rand.Next(0, 9)}{rand.Next(0, 9)}" +
                '-' +
                $"{(char)rand.Next('A', 'Z')}{(char)rand.Next('A', 'Z')}"
            );

            return strId.ToString();
        }

        public override string ToString()
        {
            return $"{Id} / {VehicleType} / {Balance}";
        }
    }
}
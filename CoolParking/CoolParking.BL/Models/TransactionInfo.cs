﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get; }
        public DateTime TransactionDate { get; }
        
        public TransactionInfo(string idVehicle, decimal sum, DateTime time)
        {           
            VehicleId = idVehicle;
            Sum = sum;
            TransactionDate = time;
        }
        
        public override string ToString()
        {
            return $"{TransactionDate}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}

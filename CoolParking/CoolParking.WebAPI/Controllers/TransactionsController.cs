﻿using System;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Models;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<TransactionInfo[]> GetLast()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                return _parkingService.ReadFromLog();
            }
            catch (InvalidOperationException)
            {
                return NotFound("File not found");
            }
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUp topUp)
        {
            if (ModelState.IsValid || !Regex.IsMatch(topUp.Id, @"\D{2}-\d{4}-\D{2}"))
            {
                return BadRequest("Body is invalid");
            }

            try
            {
                _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
                
                foreach (Vehicle vehical in _parkingService.GetVehicles())
                    if (vehical.Id == topUp.Id)
                        return vehical;
            }
            catch (ArgumentException)
            {
                return NotFound("Vehicle not found");
            }
            
            return BadRequest("Body is invalid");
        }

    }
}
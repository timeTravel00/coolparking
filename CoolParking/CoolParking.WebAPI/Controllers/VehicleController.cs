﻿using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            if(!Regex.IsMatch(id, @"\D{2}-\d{4}-\D{2}"))
                return BadRequest("Invalid Id");
            
            foreach (var vehicle in _parkingService.GetVehicles())
                if (vehicle.Id == id)                 
                    return vehicle;

            return NotFound("Vehicle not found");
        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] Vehicle vehicle)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest("Body is invalid");
            }

            _parkingService.AddVehicle(vehicle);

            return new ObjectResult(vehicle) { StatusCode = StatusCodes.Status201Created };
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult<Vehicle> DeleteVehicle(string id)
        {
            try
            {
                if(!Regex.IsMatch(id, @"\D{2}-\d{4}-\D{2}"))
                    return BadRequest("Invalid Id");

                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("Vehicle not found");
            }
        }
    }
}
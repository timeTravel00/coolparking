﻿namespace CoolParking.WebAPI.Models
{
    public class TopUp
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
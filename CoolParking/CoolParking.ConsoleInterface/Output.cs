﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.ConsoleInterface
{
    class Output
    {
        private static ParkingService parkingService = new ParkingService(
            new TimerService(), new TimerService(), new LogService("./Transactions.log"));
        
        public static void GetBalance()
        {
            Console.WriteLine($"Parking balance : {parkingService.GetBalance()}"); 
        }

        public static void GetAmountOfMoney()
        {
            decimal sum = 0;
            
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                sum += transaction.Sum;
            }
            
            Console.WriteLine($"Amount of money earned for the current period : {sum}");
        }

        public static void GetFreeAndOccupiedSpaces()
        {
            Console.WriteLine($"Number of free/occupied parking spaces : {parkingService.GetFreePlaces()}/{parkingService.GetVehicles().Count}");
        }

        public static void GetLastTransactions()
        {
            Console.WriteLine("Last transactions for the current period : ");
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine(transaction);
            }
        }

        public static void GetTransactionHistory()
        {
            Console.WriteLine($"The transaction history : {parkingService.ReadFromLog()}");
        }

        public static void GetVehicles()
        {
            Console.WriteLine("List of Vehicles located in the Parking lot : ");
            foreach (var vehicle in parkingService.GetVehicles())
            {
                Console.WriteLine(vehicle);
            }
        }

        public static void AddVehicle()
        {
            Console.WriteLine("Choice VehicleType:\n 1.PassengerCar \n 2.Truck \n 3.Bus \n 4.Motorcycle");
            if (int.TryParse(Console.ReadLine(), out int n) && (n > 0 && n < 5))
            {
                Console.WriteLine("Balance : ");
                if (decimal.TryParse(Console.ReadLine(), out decimal c) && c > 0)
                    try
                    {
                        parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(),
                            (VehicleType) n, c));
                        Console.WriteLine("Vehicle was added");
                    }
                    catch (InvalidOperationException)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("No free places");
                    }
                    catch (ArgumentException)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Car with this id has already been added");
                    }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid input!");
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid input!");
            }
        }

        public static void RemoveVehicle()
        {
            foreach (var vehicle in parkingService.GetVehicles())
            {
                Console.WriteLine(vehicle);
            }
            Console.WriteLine("Enter id:");
            try
            {
                parkingService.RemoveVehicle(Console.ReadLine());
                Console.WriteLine("Vehicle was removed");
            }
            catch (ArgumentException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id");
            }
        }

        public static void TopUpVehicleBalance()
        {
            foreach (var vehicle in parkingService.GetVehicles())
            {
                Console.WriteLine(vehicle);
            }
            
            Console.WriteLine("Enter id:");
            var id = Console.ReadLine();

            Console.WriteLine("Enter amount of money:");
            int.TryParse(Console.ReadLine(), out int sum);

            try
            {
                parkingService.TopUpVehicle(id, sum);
                Console.WriteLine("Vehicle was topped up");
            }
            catch (InvalidOperationException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Balance is negative");
            }
            catch (ArgumentException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid id");
            }
        }
        
        public static void Main()
        {
            //Menu
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Welcome to the CoolParking! \n");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Available actions: \n");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(
                "1.  Display the current Parking balance. \n" +
                "2.  Display the amount of money earned for the current period. \n" +
                "3.  Display the number of free / occupied parking spaces. \n" +
                "4.  Display last transactions for the current period. \n" +
                "5.  Display the transaction history. \n" +
                "6.  Display the list of Vehicles located in the Parking lot. \n" +
                "7.  Put the Vehicle in the Parking. \n" +
                "8.  Pick up the Vehicle from Parking. \n" +
                "9.  Top up vehicle balance. \n" +
                "10. Exist \n");
            
            int choice;

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("\nSelect an action using a number");
                Console.ResetColor();

                while (true)
                    if (int.TryParse(Console.ReadLine(), out int number) && (number > 0 && number < 11))
                    {
                        choice = number;
                        break;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Invalid input!");
                        Console.ResetColor();
                    }

                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;

                switch (choice)
                {
                    case 1: GetBalance();
                            break;
                    case 2: GetAmountOfMoney();
                            break;
                    case 3: GetFreeAndOccupiedSpaces();
                            break;
                    case 4: GetLastTransactions();
                            break;
                    case 5: GetTransactionHistory();
                            break;
                    case 6: GetVehicles();
                            break;
                    case 7: AddVehicle();
                            break;
                    case 8: RemoveVehicle();
                            break;
                    case 9: TopUpVehicleBalance();
                            break;
                    case 10: Environment.Exit(0); 
                            break;
                }
                Console.ResetColor();
            }
        }
    }
}